﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp135
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Exercies :: To count the total number of words in a string ");
            Console.WriteLine(new string('_', 30));
            Console.Write("Input Sentence - ");
            string Word = Convert.ToString(Console.ReadLine());
            int count = 0;
            foreach (char i in Word)
            {
                if (i == ' ' || i == '\n'  || i == '\t' )
                {
                    count++;
                }
            }
            Console.WriteLine($"Sentenc's Words quantity = {count + 1}");
        }
    }
}
